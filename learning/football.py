# 3
# Зенит;3;Спартак;1
# Спартак;1;ЦСКА;1
# ЦСКА;0;Зенит;2
# Вывод программы необходимо оформить следующим образом:
# Команда:Всего_игр Побед Ничьих Поражений Всего_очков
x = int(input())
i = 0
a = []
while i < x:
    b = input().split(';')
    a.append(b)
    i += 1
team = {}
countGames = 0

for i in a:
    for y in range(len(i)):
        if len(i[y]) > 2:
            if i[y] not in team:
                team[i[y]] = []

#Get count game for teams
for i in team.keys():
    count = 0
    for y in a:
        for z in y:
            if i == z:
                count += 1
    team[i].append(count)

# Get game information
for i in team.keys():
    victory = 0
    loose = 0
    defeat = 0
    game_points = 0
    for y in range(len(a)):
        if i in a[y]:
            if i == a[y][0]:
                if int(a[y][1]) > int(a[y][3]):
                    victory += 1
                    game_points += 3
                elif int(a[y][1]) == int(a[y][3]):
                    defeat += 1
                    game_points += 1
                elif int(a[y][1]) < int(a[y][3]):
                    loose += 1
            else:
                if int(a[y][1]) > int(a[y][3]):
                    loose += 1
                elif int(a[y][1]) == int(a[y][3]):
                    defeat += 1
                    game_points += 1
                elif int(a[y][1]) < int(a[y][3]):
                    victory += 1
                    game_points += 3

    team[i].append(victory)
    team[i].append(defeat)
    team[i].append(loose)
    team[i].append(game_points)
#Show result
for key,value in team.items():
    tr = ''
    for i in value:
        tr += str(i) + " "
    print(key+":"+tr)
