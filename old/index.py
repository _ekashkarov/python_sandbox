# -*- coding: utf-8 -*-
text = 'русский'
print (text)

n = range(3, 60, 10)
print (n)

arg = [3, 6]
n1 = range(*arg)
print (n1)

# Замыкание

def make_adder(x):
    def adder(n):
        return x+n
    return adder

adder = make_adder(10)
print (adder(5))

#filter
list0 = [1, -10, 15, 4, 7]
test_filter = filter(lambda x: x<8, list0)
print (test_filter)

# map
list1 = [7, 2, 10]
list2 = [1, 5, 4]
test_map = map(lambda x,y: x*y, list1, list2)
print (test_map)

#reduce
list3 = [2, 3, 4, 5, 6]
# test_reduce = reduce(lambda res, x: res*x, list3, 1)
# print (test_reduce)