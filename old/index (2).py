# -*- coding: utf-8 -*-

# x = int(raw_input("Введите число: "))
# if x < 0:
#     print 'отрицательное'
# elif x==0:
#     print '0'
# elif x==1:
#     print '1'
# else:
#     print 'Больше 1'

list = ['one', 'two', 'three', 'four', 'five', 'six', 'seven']
for x in list:
    print (x)

#Range не использовать для больших диапазонов
list_range = range(0, 10, 2)
print (list_range)

# a = ['Что-то', 'новенькое', '?']
# for i in xrange(len(a)):
#     print (a[i])

#Import all variables from file
from old.test_import import*
print (long_variable_3)

#Import one variable from file
from old.test_import import long_variable
print( long_variable)

#Import one variable from file as
from old.test_import import long_variable as name
print(name)